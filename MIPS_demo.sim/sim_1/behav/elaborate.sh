#!/bin/bash -f
xv_path="/home/edsp_lab/Xilinx/Vivado/2016.4"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xelab -wto 92ff84ff7c1243de9a1c3e881fe44990 -m64 --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot TB_InstructionMemory_behav xil_defaultlib.TB_InstructionMemory -log elaborate.log
