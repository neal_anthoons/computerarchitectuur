#!/bin/bash -f
xv_path="/home/edsp_lab/Xilinx/Vivado/2016.4"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xsim TB_InstructionMemory_behav -key {Behavioral:sim_1:Functional:TB_InstructionMemory} -tclbatch TB_InstructionMemory.tcl -log simulate.log
