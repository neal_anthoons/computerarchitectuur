----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/22/2019 10:09:30 AM
-- Design Name: 
-- Module Name: TB_InstructionMemory - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TB_InstructionMemory is
end TB_InstructionMemory;

architecture Behavioral of TB_InstructionMemory is
    component InstructionMemory
    port(clk     : in std_logic;
         Adress  : in std_logic_vector(31 downto 0);
         OPcode  : out std_logic_vector(31 downto 0));
    end component;
    
    signal clk: std_logic := '0';
    signal Adress : std_logic_vector(31 downto 0) := (others => '0');        
    
    signal OPcode : std_logic_vector(31 downto 0);
    
    constant clk_period : time := 10 ns;

BEGIN    
    uut: InstructionMemory port map(
        clk => clk,
        Adress => Adress,
        OPcode => OPcode
    );
    
    clk_process: process
    begin
        clk <= '0';
        wait for clk_period/2;                
        clk <= '1';
        wait for clk_period/2;
    end process;
    
    stim_proc: process
    begin 
        wait for 100 ns;
        Adress <= X"00000012";                        --Instructie 1
        wait for clk_period;
        Adress <= X"00000008";                        --Instructie 2
        
        wait for clk_period*10;
        Adress <= X"00000004";
    end process;
END;
