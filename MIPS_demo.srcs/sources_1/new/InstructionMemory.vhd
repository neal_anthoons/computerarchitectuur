----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/22/2019 09:40:55 AM
-- Design Name: 
-- Module Name: InstructionMemory - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity InstructionMemory is
    Port ( clk       : in STD_LOGIC;
           Adress    : in STD_LOGIC_VECTOR(31 downto 0);            -- PC_OUT
           OPcode    : out STD_LOGIC_VECTOR(31 downto 0));
end InstructionMemory;

architecture Behavioral of InstructionMemory is
begin

MemoryPC: process(clk)
    subtype word is STD_LOGIC_VECTOR(31 downto 0);
    type memory is array(0 to 127) of word;         --maakt een klasse van 128 words (32 bit) => 32*4 = 128
    variable myMemory:memory :=                     --maakt instance van de klasse, := omdat het een variabele is
        ("00000000000000000000000000000000", 
         "00000000000000000000000000000001",
         "00000000000000000000000000000010", 
         "00000000000000000000000000000011",
         "00000000000000000000000000000100", 
         "00000000000000000000000000000101",
         others => (others=>'0'));
         
begin    
    if(rising_edge(clk)) then       
        OPcode <= myMemory(conv_integer(Adress(31 downto 2)));      -- downwto 2, omdat instructie 4 bytes is.
                                                                    -- Dit is equivalent aan delen door 4, wat bekomen kan worden door de twee LSB weg te laten 
    end if;
end process MemoryPC;

end Behavioral;
